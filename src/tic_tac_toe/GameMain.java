package tic_tac_toe;

import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.entities.User;
import utils.Printer;
import utils.Utils;
/**
 * The main class for the Tic-Tac-Toe (Console-OO, non-graphics version)
 * It acts as the overall controller of the game.
 */
public class GameMain {
	private Board board;            // the game board
	private GameState currentState; // the current state of the game (of enum GameState)
	private Seed currentPlayer;     // the current player (of enum Seed)

	private Utils p1;
	private Utils p2;
	private TextChannel textChannel;


	/** Constructor to setup the game */
	public GameMain(Utils p1, Utils p2, TextChannel textChannel) {
		board = new Board();  // allocate game-board



		// Initialize the game-board and current status
		initGame(p1, p2, textChannel);
		// Play the game once. Players CROSS and NOUGHT move alternately.
		Printer.sendMessage(textChannel, board.paint());
		do {
			playerMove(currentPlayer); // update the content, currentRow and currentCol
			Printer.sendMessage(textChannel, board.paint());
			updateGame(currentPlayer); // update currentState
			// Print message if game-over
			if (currentState == GameState.CROSS_WON) {
				Printer.sendMessage(this.textChannel, p1.getUser().getName() + " won !");
			} else if (currentState == GameState.NOUGHT_WON) {
				Printer.sendMessage(this.textChannel, p2.getUser().getName() + " won !");
			} else if (currentState == GameState.DRAW) {
				Printer.sendMessage(this.textChannel, "It's Draw! Bye!");
			}
			// Switch player
			currentPlayer = (currentPlayer == Seed.CROSS) ? Seed.NOUGHT : Seed.CROSS;
		} while (currentState == GameState.PLAYING);  // repeat until game-over
	}

	/** Initialize the game-board contents and the current states */
	public void initGame(Utils p1, Utils p2, TextChannel textChannel) {
		board.init();  // clear the board contents
		currentPlayer = Seed.CROSS;       // CROSS plays first
		currentState = GameState.PLAYING; // ready to play
		this.p1 = p1;
		this.p2 = p2;
		this.textChannel = textChannel;
	}

	/** The player with "theSeed" makes one move, with input validation.
       Update Cell's content, Board's currentRow and currentCol. */
	public void playerMove(Seed theSeed) {
		boolean validInput = false;  // for validating input
		Utils playing;
		Integer row = null, col = null;
		String buffer;
		do {
			if (theSeed == Seed.CROSS) {
				buffer = "Player " + p1.getUser().getName() + " : \n";
				playing = p1;
			} else {
				buffer = "Player " + p2.getUser().getName() + " : \n";
				playing = p2;
			}

			do {
				do {
					try {
						buffer += "enter your move (row[1-3])";
						Printer.sendMessage(textChannel, buffer);
						buffer = "";
						row = Integer.parseInt(playing.readString()) - 1;
					} catch (Exception e){
						Printer.sendMessage(textChannel, "Please send an integer");
					}
				}
				while (row == null);
			} while (!(row.intValue() >= 0 && row.intValue() < Board.ROWS));

			do {
				do {
					try {
						buffer += "enter your move (column[1-3])";
						Printer.sendMessage(textChannel, buffer);
						buffer = "";
						col = Integer.parseInt(playing.readString()) - 1;
					} catch (Exception e){
						Printer.sendMessage(textChannel, "Please send an integer");
					}
				} while (col == null);
			} while (!(col.intValue() >= 0 && col.intValue() < Board.COLS));

			if (row.intValue() >= 0 && row.intValue() < Board.ROWS && col.intValue() >= 0 && col.intValue() < Board.COLS
					&& board.cells[row][col].content == Seed.EMPTY) {
				board.cells[row][col].content = theSeed;
				board.currentRow = row;
				board.currentCol = col;
				validInput = true; // input okay, exit loop
			} else {
				Printer.sendMessage(textChannel, "This move at (" + (row + 1) + "," + (col + 1)
						+ ") is not valid. Try again...");
			}
		} while (!validInput);   // repeat until input is valid
	}

	/** Update the currentState after the player with "theSeed" has moved */
	public void updateGame(Seed theSeed) {
		if (board.hasWon(theSeed)) {  // check for win
			currentState = (theSeed == Seed.CROSS) ? GameState.CROSS_WON : GameState.NOUGHT_WON;
		} else if (board.isDraw()) {  // check for draw
			currentState = GameState.DRAW;
		}
		// Otherwise, no change to current state (still GameState.PLAYING).
	}
}