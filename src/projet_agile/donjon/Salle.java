package projet_agile.donjon;


import java.util.ArrayList;

import net.dv8tion.jda.api.entities.TextChannel;
import projet_agile.evenement.*;
import projet_agile.personnage.*;
import utils.Printer;
import utils.Utils;
import projet_agile.objet.*;

public class Salle {
	private int numeroSalle = 0;
	private Objet objet;
	private Evenement contientEvenement;
	private boolean numeroIsDefined;
	public boolean estVisite;

	private ArrayList<Personnage> monstre;
	public boolean salleBoss;
	
	private TextChannel textChannel;
	private Utils utils;

	Salle(int numeroSalle, double txAppEnnemi, double txAppObjet, double txAppEvent, TextChannel textChannel, Utils utils) {
		this.numeroSalle = numeroSalle;
		estVisite = false;
		objet = null;
		contientEvenement = null;
		numeroIsDefined = false;
		monstre = new ArrayList<>();
		this.textChannel = textChannel;
		this.utils = utils;
		initialiseSalle(txAppEnnemi, txAppObjet, txAppEvent);
	}

	Salle(int numeroSalle, double txAppObjet, TextChannel textChannel, Utils utils) {
		this(numeroSalle, 0.8, txAppObjet, 0.25, textChannel, utils);
	}

	Salle(int numeroSalle, double txAppObjet, double txAppEvent, TextChannel textChannel, Utils utils) { 
		this(numeroSalle, 0.8, txAppObjet, txAppEvent, textChannel, utils);
	}

	Salle(int numeroSalle, TextChannel textChannel, Utils utils) {
		this(numeroSalle, 0.8, 0.25, 0.4, textChannel, utils);
	}

	private void initialiseSalle(double txAppEnnemi, double txAppObjet, double txAppEvent) {
		if (Math.random() < txAppObjet) initialiseObjet();
		if (Math.random() < txAppEnnemi) initialiseMonstre();
		if (Math.random() < txAppEvent) initialiseEvenement();
	}

	public void initialiseMonstre() {
		double chance = Math.random();
		if (chance < 0.4) monstre.add(new SqueletteBasique());
		else monstre.add(new ChevalierSquelette());
	}

	public void initialiseEvenement() {
		int rdm = (int)(Math.random()*21);

		if(rdm<=5) contientEvenement = new Coffre(textChannel, utils);
		else if(rdm<=10) contientEvenement = new Table(textChannel);
		else if (rdm <= 15) contientEvenement = new FontaineRevigorante(textChannel);
		else if(rdm <= 17) contientEvenement = new Rocher(textChannel, utils);
		else contientEvenement = new RatelierArmes(textChannel);
	}

	public boolean contientEvenement() {
		return contientEvenement != null;
	}

	public Evenement getEvenement() {
		return contientEvenement;
	}

	public Personnage getMonstre() {
		return monstre.get(0);
	}

	public void setSalleBoss(boolean b) {
		salleBoss = b;
	}

	public void mortEnnemi() {
		monstre.remove(0);
	}

	public boolean contientEnnemi() {
		return !monstre.isEmpty();
	}

	public void addMonstre(Personnage monstre) {
		this.monstre.add(monstre);
	}

	public void setNumeroSalle(int numero) {
		if (!numeroIsDefined) {
			this.numeroSalle = numero;
		}
	}

	public int getNombreEnnemi() {
		return monstre.size();
	}

	public int getNumeroSalle() {
		return numeroSalle;
	}

	public boolean getEstVisite() {
		return estVisite;
	}

	public boolean salleBoss() {
		return monstre.size()>1;
	}

	public String contenuSalle() {
		String msg = "";
		/**
		 * if(contientObjet) msg += "Cette salle contient un objet ! \n"; else msg +=
		 * "Cette salle ne contient pas d'objet !\n";
		 **/

		if (contientEnnemi())

			msg += "\t\t>> Cette salle contient un ennemi !\n";
		else
			msg += "\t\t>> Cette salle ne contient pas de monstre !\n";


		if (contientEnnemi())

			msg += "\t\t>> Un evenement inatendu se produit !\n";
		else
			msg += "\t\t>> Rien de spécial ne s'est produit !\n";

		return msg;
	}

	public boolean contientUnObjet() {
		return objet != null;
	}

	public Objet fouille() {
		if (!contientUnObjet()) {
			Printer.sendMessage(textChannel, "\t>> Vous n'avez rien trouv�");
			return null;
		}
		else 
			Printer.sendMessage(textChannel, "\t>>Vous avez trouv� " + objet.getNom() + "");
		Objet objetTmp = objet;
		objet = null;
		return objetTmp;

	}

	public void initialiseObjet(Objet objet) {
		this.objet = objet;
	}

	private void initialiseObjet() {
		int alea = (int) (Math.random() * 100);


		if (alea <= 10) objet = new EpeeAvance();
		else if (alea <= 15) objet = new ArcAvance();
		else if (alea <= 20) objet = new PotionGeante();
		else if (alea <= 55) objet = new PotionMoyenne();
		else if (alea <= 60) objet = new BatonAvance();

		else objet = new PotionSimple();
	}

	public void supprimerEvenement()
	{
		this.contientEvenement = null;
	}
}
