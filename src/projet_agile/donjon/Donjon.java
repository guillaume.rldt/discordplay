package projet_agile.donjon;

import java.awt.Point;

import net.dv8tion.jda.api.entities.TextChannel;
import projet_agile.objet.*;

import projet_agile.personnage.Dragon;
import utils.Utils;

public class Donjon {
	private Salle[][] sallesDuDonjon;
	private Salle salleActuelle;
	private TextChannel textChannel;
	private Utils utils;
	
	
	public Donjon(TextChannel textChannel, Utils utils) {
		this.textChannel = textChannel;
		this.utils = utils;
		this.init();
	}

	private void init() {
		sallesDuDonjon = new Salle[5][4];

		sallesDuDonjon[0][0] = null;
		sallesDuDonjon[0][1] = new Salle(0, 0, 1, 1, textChannel, utils);
		sallesDuDonjon[0][2] = null;
		sallesDuDonjon[0][3] = null;
		sallesDuDonjon[1][0] = null;
		sallesDuDonjon[1][1] = new Salle(1, 0.1, 0.2, textChannel, utils);
		sallesDuDonjon[1][2] = new Salle(2, 0.8, 0, textChannel, utils);
		sallesDuDonjon[1][3] = new Salle(3, 0.5, 0.8, textChannel, utils);
		sallesDuDonjon[2][0] = new Salle(4, 0.2, 0.4, textChannel, utils);
		sallesDuDonjon[2][1] = new Salle(5, 0.6, 0.2, textChannel, utils);
		sallesDuDonjon[2][2] = new Salle(6, 0.01, 0.6, textChannel, utils);
		sallesDuDonjon[2][3] = new Salle(7, 0.98, 0.1, textChannel, utils);
		sallesDuDonjon[3][0] = new Salle(8, 0.666, 0.9, textChannel, utils);
		sallesDuDonjon[3][1] = new Salle(9, 0.42, 0.33, textChannel, utils);
		sallesDuDonjon[3][2] = new Salle(10, 0, 1, 1, textChannel, utils);
		sallesDuDonjon[3][3] = null;
		sallesDuDonjon[4][0] = null;
		sallesDuDonjon[4][1] = null;
		sallesDuDonjon[4][2] = new Salle(11, 0.8, 0, 0, textChannel, utils);
		sallesDuDonjon[4][3] = null;

		sallesDuDonjon[0][1].initialiseObjet(new PotionSimple());
		sallesDuDonjon[3][2].initialiseObjet(new PotionMoyenne());

		sallesDuDonjon[4][2].setSalleBoss(true);
		sallesDuDonjon[4][2].addMonstre(new Dragon());

		this.salleActuelle = sallesDuDonjon[0][1];
		this.salleActuelle.estVisite = true;
	}

	public String toString() {
		String res = "\n D O N J O N\n";

		for (int i = 0; i < sallesDuDonjon.length; i++) {
			for (int j = 0; j < sallesDuDonjon[0].length; j++) {
				if (sallesDuDonjon[i][j] == null)
					res += " \u25FC ";
				else if (sallesDuDonjon[i][j] == salleActuelle)
					res += " \u25A3 ";
				else if (sallesDuDonjon[i][j].getEstVisite())
					res += " \u25A8 ";
				else
					res += " \u25FB ";
			}
			res += "\n";
		}

		return res + "";
	}

	public Point chercherSalle(int salle) {

		for (int i = 0; i < sallesDuDonjon.length; i++) {
			for (int j = 0; j < sallesDuDonjon[0].length; j++) {
				if (sallesDuDonjon[i][j] != null)
					if (salle == sallesDuDonjon[i][j].getNumeroSalle())
						return new Point(i, j);
			}
		}
		return null;
	}

	public Salle getSalleActuelle() {
		return this.salleActuelle;
	}

	public Salle[][] getSallesDuDonjon() {
		return sallesDuDonjon;
	}

	public void setSallesDuDonjon(Salle[][] sallesDuDonjon) {
		this.sallesDuDonjon = sallesDuDonjon;
	}

	public void setSalleActuelle(Salle salleActuelle) {
		this.salleActuelle = salleActuelle;
	}
	
	public boolean toutVisite() {
		for(int i=0; i<sallesDuDonjon.length; i++) {
			for(int j=0; j<sallesDuDonjon[0].length; j++) {
				if(sallesDuDonjon[i][j] != null && !sallesDuDonjon[i][j].getEstVisite())
					return false;
			}
		}
		return true;
	}

	
	//A garder dans le cas ou l'on souhaite placer la salle du boss aléatoirement
	
	/*private void setSalleBoss() {
		int salleBossX = (int)(Math.random()*4), salleBossY;
		if(salleBossX == 3) {
			salleBossY = (int)(Math.random()*2+1);
		} else {
			salleBossY = 3;
		}
		
		sallesDuDonjon[salleBossY][salleBossX].addMonstre(new Dragon());
		sallesDuDonjon[salleBossY][salleBossX].setSalleBoss(true);
	}*/

}
