package projet_agile.personnage;

import projet_agile.objet.EpeeSimple;

public class Chevalier extends Personnage{
	
	public Chevalier() {
		super("Chevalier", 5, 5, 5, 5);
		fo = 60;
		ad = 25;
		intel = 25;
		tri = 55;
		baseHp = baseHp + tri/5;
		hp = baseHp;
		armeEquipe = new EpeeSimple();
	}
	
	@Override
	public String toString() {
		return  "\\             //\n" +
				" \\\\           _!_\n" + 
				"  \\\\         /___\\\n" + 
				"   \\\\        [+++]\n" + 
				"    \\\\    _ _\\^^^/_ _\n" + 
				"     \\\\/ (    '-'  ( )\n" + 
				"     /( \\/ | {&}   /\\ \\\n" + 
				"       \\  / \\     / _> )\n" + 
				"        \"`   >:::;-'`\"\"'-.\n" + 
				"            /:::/         \\\n" + 
				"           /  /||   {&}   |\n" + 
				"          (  / (\\         /\n" + 
				"          / /   \\'-.___.-'\n" + 
				"        _/ /     \\ \\\n" + 
				"       /___|    /___|";
	}
	
}
