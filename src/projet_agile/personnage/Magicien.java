package projet_agile.personnage;

import projet_agile.objet.BatonSimple;

public class Magicien extends Personnage {
	
	public Magicien(int fo, int ad, int intel, int tri) {
		super("Magicien", fo, ad, intel, tri);
		hp = hp + tri/5;
	}
	
	public Magicien() {
		super("Magicien", 5, 5, 5, 5);
		fo = 20;
		ad = 20;
		intel = 80;
		tri = 20;
		baseHp = baseHp + tri/5;
		hp = baseHp - 10;
		armeEquipe = new BatonSimple();

	}
	
	@Override
	public String toString() {
		return "         /^\\   \n" +
				"    /\\   \"V\"\n" + 
				"   /__\\   I    \n" + 
				"  //..\\\\  I  \n" + 
				"  \\].`[/  I\n" + 
				"  /l\\/j\\  (]  \n" + 
				" /. ~~ ,\\/I   \n" + 
				" \\\\L__j^\\/I  \n" + 
				"  \\/--v}  I   \n" + 
				"  |    |  I  \n" + 
				"  |    |  I \n" + 
				"  |    l  I   \n" + 
				"_/j  L l\\_!  ";
	}
}
