package projet_agile.personnage;


public class SqueletteBasique extends Personnage {
	
	public SqueletteBasique() {
		super("Squelette", 20, 20, 20, 20);
		baseHp = baseHp + tri/5;
		hp = baseHp;
	}
	
	@Override
	public String toString() {
		return  "               _.---._\n" +
				"             .'       `.\n" + 
				"             :)       (:\n" + 
				"             \\ (@) (@) /\n" + 
				"              \\   A   /\n" + 
				"               )     (\n" + 
				"               \\\"\"\"\"\"/\n" + 
				"                `._.'\n" + 
				"                 .=.\n" + 
				"         .---._.-.=.-._.---.\n" + 
				"        / ':-(_.-: :-._)-:` \\\n" + 
				"       / /' (__.-: :-.__) `\\ \\\n" + 
				"      / /  (___.-` '-.___)  \\ \\\n" + 
				"     / /   (___.-'^`-.___)   \\ \\\n" + 
				"    / /    (___.-'=`-.___)    \\ \\\n" + 
				"   / /     (____.'=`.____)     \\ \\\n" + 
				"  / /       (___.'=`.___)       \\ \\\n" + 
				" (_.;       `---'.=.`---'       ;._)\n" + 
				" ;||        __  _.=._  __        ||;\n" + 
				" ;||       (  `.-.=.-.'  )       ||;\n" + 
				" ;||       \\    `.=.'    /       ||;\n" + 
				" ;||        \\    .=.    /        ||;\n" + 
				" ;||       .-`.`-._.-'.'-.       ||;\n" + 
				".:::\\      ( ,): O O :(, )      /:::.\n" + 
				"|||| `     / /'`--'--'`\\ \\     ' ||||\n" + 
				"''''      / /           \\ \\      ''''\n" + 
				"         / /             \\ \\\n" + 
				"        / /               \\ \\\n" + 
				"       / /                 \\ \\\n" + 
				"      / /                   \\ \\\n" + 
				"     / /                     \\ \\\n" + 
				"    /.'                       `.\\\n" + 
				"   (_)'                       `(_)\n" + 
				"    \\\\.                       .//\n" + 
				"     \\\\.                     .//\n" + 
				"      \\\\.                   .//\n" + 
				"       \\\\.                 .//\n" + 
				"        \\\\.               .//\n" + 
				"         \\\\.             .//\n" + 
				"          \\\\.           .//\n" + 
				"          ///)         (\\\\\\\n" + 
				"        ,///'           `\\\\\\,\n" + 
				"       ///'               `\\\\\\\n" +
				"      \"\"'                   '\"\"";
	}
}
