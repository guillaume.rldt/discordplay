package projet_agile.personnage;

import java.util.ArrayList;
import java.util.Random;

import net.dv8tion.jda.api.entities.TextChannel;
import projet_agile.objet.*;
import utils.Printer;

public abstract class Personnage {

	private ArrayList<Objet> inventaire = new ArrayList<Objet>();
	protected String nom;
	protected int hp;
	protected int baseHp = 50;
	protected int pa;//Points Actions
	protected int fo;//force = +degats
	protected int ad;//adresse = +precision/esquive 
	protected int intel;//intellignece = +pa
	protected int tri;//trippe = +sante/
	protected boolean mort;
	protected Arme armeEquipe;

	public Personnage(String nom, int fo, int ad, int intel, int tri) {
		this.nom = nom;
		hp = baseHp;
		pa = 4;
		this.fo = fo;
		this.ad = ad;
		this.intel = intel;
		this.tri = tri;
		this.mort = false;
		this.armeEquipe = new EpeeSimple();
	}

	public int getHp() {
		return hp;
	}

	public void setHp(int hp) {
		this.hp = hp;
	}

	public int getPa() {
		return pa;
	}

	public void setPa(int pa) {
		this.pa = pa;
	}

	public int getFo() {
		return fo;
	}

	public void setFo(int fo) {
		this.fo = fo;
	}

	public int getAd() {
		return ad;
	}

	public void setAd(int ad) {
		this.ad = ad;
	}

	public int getIntel() {
		return intel;
	}

	public void setIntel(int intel) {
		this.intel = intel;
	}

	public int getTri() {
		return tri;
	}

	public void setTri(int tri) {
		this.tri = tri;
	}
	public boolean estMort() {
		return mort;
	}

	public String getNom()
	{
		return nom;
	}

	@Override
	public String toString() {
		return "Personnage [hp=" + hp + ", pa=" + pa + ", fo=" + fo + ", ad=" + ad + ", intel=" + intel + ", tri=" + tri
				+ "]";
	}

	public String getCaracteristiques() {
		return "Vous avez "+hp+" PV, "+fo+" en force, "+ad+" en adresse, "+intel+" en intelligence et "+tri+" en courage";
	}

	public String attaque(Personnage p) {
		Random alea = new Random();
		String message = "";
		int degat = this.fo/5 + armeEquipe.getFo() - p.tri/10 + alea.nextInt(3);

		if ((this.ad + 40) > alea.nextInt(100) && p.ad < alea.nextInt(100)) {
			p.setHp(p.getHp() - degat);
			message += "\t\t> " + p.getNom() + " subit " + degat + " d�g�ts !";
		}
		else  { 
			message += "\t\t> " + this.getNom() + " se rate comme un boloss !";
		}

		if(p.getHp() <= 0) {
			p.mort = true;
			message += "\t\t> " + p.getNom() + " est mort !";
		}

		else { 
			message += "\t\t> " + p.getNom() + " a encore " + p.getHp() + "PV";
		}
		return message;
	}

	public boolean soin(int heal) {
		hp += heal;

		return false;
	}

	public boolean add(Objet objet, TextChannel textChannel) {
		if (objet == null) return false;
		Printer.sendMessage(textChannel, "\t>> L'objet a �t� ajouter dans votre inventaire");
		return inventaire.add(objet);
	}

	public String showInventaire() {
		String res = "";
		res += "\t\t| Inventaire :\n";
		if (inventaire.size() == 0) res += "\t\t|  Rien";
		for (int i = 0; i < inventaire.size(); i++) {
			res += "\t\t| [" + i + "] " + inventaire.get(i).toString() + "\n";
		}
		return res;
	}

	public boolean useObject(int index, TextChannel textChannel)
	{
		if(inventaire.get(index).getClass().getSuperclass() == Potion.class)
		{
			Potion potionTmp = (Potion) inventaire.get(index);
			soin(potionTmp.soin);
			inventaire.remove(index);
			Printer.sendMessage(textChannel, "\t>> Vous avez r�g�n�r� " + potionTmp.soin + " PV !");
			Printer.sendMessage(textChannel, "\t>> Vous avez maintenant " + hp + " PV !");
			return true;
		}
		else if(inventaire.get(index).getClass().getSuperclass() == Arme.class)
		{

			if(armeEquipe != null) inventaire.add(armeEquipe);

			armeEquipe = (Arme) inventaire.get(index);
			Printer.sendMessage(textChannel, "\t>> Vous avez �quip� " + armeEquipe.toString() + " !");
			inventaire.remove(index);
			return true;
		}
		return false;
	}

}
