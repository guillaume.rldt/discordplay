package projet_agile.evenement;

import java.util.Random;

import net.dv8tion.jda.api.entities.TextChannel;
import projet_agile.objet.*;
import projet_agile.personnage.Personnage;
import utils.Printer;

public class Table extends Evenement {
	
	private TextChannel textChannel;
	
	public Table(TextChannel textChannel) {
		super("\t>> Une myst�rieuse table semble ancr�e dans le coin de la pi�ce.\n\t>> Elle est remplie d'outils d'alchimiste.", 10);
		this.textChannel = textChannel;
	}
	
	public void effect(Personnage personnage) {
		Printer.sendMessage(textChannel, this.annonce);
		Random alea = new Random();
		int chance = alea.nextInt(100);
		if(chance < 25) {
			Printer.sendMessage(textChannel, "\t>> Vous ne trouvez rien d'int�ressant. L'alchimiste a du prendre les objets de valeur.");
		}
		if(chance >= 25 && chance < 75) {
			Printer.sendMessage(textChannel, "\t>> Un accident s'est produit lors de la fouille ! Vous subissez 4 points de d�g�ts.");
			personnage.setHp(personnage.getHp()-4);
		}
		if(chance >= 75) {
			Printer.sendMessage(textChannel, "\t>> Dans la pr�cipitation, l'alchimiste a laiss� derri�re lui quelques unes de ses babioles. Vous r�cup�rez une potion de soin.");
			personnage.add(new PotionMoyenne(), textChannel);

		}
	}
}
