package projet_agile.evenement;

import net.dv8tion.jda.api.entities.TextChannel;
import projet_agile.personnage.Personnage;
import utils.Printer;

public class FontaineRevigorante extends Evenement {
	
	private TextChannel textChannel;
	
	public FontaineRevigorante(TextChannel textChannel) {
		super("\t>> Une fontaine revigorante si�ge au milieu de la pi�ce. Ici, le temps semble s'�tre arr�t�. Vous trouvez cela presque reposant.\n\t>> Vous regagnez 15 points de vie.", 15);
		this.textChannel = textChannel;
	}
	
	public void effect(Personnage personnage) {
		Printer.sendMessage(textChannel, this.annonce);
		personnage.setHp(personnage.getHp()+15);
	}
}
