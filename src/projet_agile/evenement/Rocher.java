package projet_agile.evenement;

import java.util.Random;

import net.dv8tion.jda.api.entities.TextChannel;
import projet_agile.objet.*;
import projet_agile.personnage.*;
import utils.Printer;
import utils.Utils;

public class Rocher extends Evenement{

	private TextChannel textChannel;
	private Utils utils;

	public Rocher(TextChannel textChannel, Utils utils) {
		super("\t>> Un pi�destal �merge du sol au milieu de la salle, enfermant en son sein une �p�e � la poign�e ouvrag�", 17);
		this.textChannel = textChannel;
	}

	public void effect(Personnage personnage) {
		Printer.sendMessage(textChannel, this.annonce);
		Random alea = new Random();
		int des = alea.nextInt(100);
		Objet objet = new EpeeSacre();
		Printer.sendMessage(textChannel, "\t>> Voulez vous essayer de vous en emparer ? y/n");
		switch (utils.readString()) {
		case "y":

			if (des > 90 - personnage.getTri()) {
				Printer.sendMessage(textChannel, "\t>> Les dieux sont de votre c�t�, et vous parvenez � sortir l'" + objet.getNom() + " de son fourreau de pierre.");
				personnage.add(objet, textChannel);
			}
			else if (des > 10) {
				Printer.sendMessage(textChannel, "\t>> Vous tirez de toute votre force, en vain, l'��e ne bouge pas d'un pouce.");
			}
			else { 
				Printer.sendMessage(textChannel, "\t>> Vous tirez de toute votre force lorsque vous entendez un craquement sinistre, vous n'�tes plus tout jeune et votre dos vous fait souffrir, vous perdez deux PV.");
				personnage.setHp(personnage.getHp() - 2);;	}
			break;

		default:
			break;
		}
	}
}
