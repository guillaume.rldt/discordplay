package projet_agile.evenement;

import java.util.Random;

import net.dv8tion.jda.api.entities.TextChannel;
import projet_agile.main.*;
import projet_agile.objet.*;
import projet_agile.personnage.*;
import utils.Printer;
import utils.Utils;

public class Coffre extends Evenement{

	private Objet objet;
	private TextChannel textChannel;
	private Utils utils;

	public Coffre(TextChannel textChannel, Utils utils) {
		super("\t>> Vous apercevez un coffre dissimulez dans la pénombre de la pi�ce, il brille !", 5);
		this.textChannel = textChannel;
		this.utils = utils;
	}

	public void effect(Personnage personnage) {
		Printer.sendMessage(textChannel, this.annonce);
		Random alea = new Random();
		if (alea.nextInt(100) <= 10) {
			objet = new EpeeAvance();

		}

		else objet = new PotionMoyenne();

		int des = alea.nextInt(100);
		if (des > 25) {
			Printer.sendMessage(textChannel, "\t>> Le coffre est l�g�rement entrouvert, vous pouvez sentir l'�quipement qui vous tend les bras !");
			des = alea.nextInt(100);
			Printer.sendMessage(textChannel, "\t>> Voulez vous l'ouvrir ? y/n");
			switch (utils.readString()) {
			case "y":

				if (des > 25) {
					Printer.sendMessage(textChannel, "\t>> La chance vous sourit, vous trouvez un(e) " + objet.getNom() + " et vous en emparez.");
					personnage.add(objet, textChannel);
				}
				else if (des > 10) {
					Printer.sendMessage(textChannel, "\t>> Damn� soit les imb�ciles pass� avant vous ! Le coffre est vide...");
				}
				else { 
					Printer.sendMessage(textChannel, "\t>> Mal�diction ! Un mimic !");
					Combat.combat(personnage, new Mimic(), textChannel, utils);	
				}
				break;

			default:
				break;
			}
		}
		else {
			Printer.sendMessage(textChannel, "\t>>Le coffre est ferm�, mais vous �tes confiant sur vos chance de forcer la serrure");
			des = alea.nextInt(100);
			Printer.sendMessage(textChannel, "\t>> Voulez vous l'ouvrir ? (y/n)");
			switch(utils.readString()) {
			case "y":

				if (des > 25) {
					Printer.sendMessage(textChannel, "\t>> Que celui qui � pi�g� ce coffre pourrissent dans les limbes, vous avez subit quatre point de d�g�t.");
					personnage.setHp(personnage.getHp() - 4);
				}
				else if (des > 10) {
					Printer.sendMessage(textChannel, "\t>>La chance vous sourit, vous trouvez un(e) " + objet.getNom() + " et vous vous en emparez !");
					personnage.add(objet, textChannel);
				}
				else { 
					Printer.sendMessage(textChannel, "\t>> Mal�diction ! Un mimic !");
					Combat.combat(personnage, new Mimic(), textChannel, utils);	
					}
				break;

			default:
				break;

			}
		}
	}
}

