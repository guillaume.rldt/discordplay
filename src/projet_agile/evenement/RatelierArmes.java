package projet_agile.evenement;

import java.util.Random;

import net.dv8tion.jda.api.entities.TextChannel;
import projet_agile.objet.*;
import projet_agile.personnage.*;
import utils.Printer;

public class RatelierArmes extends Evenement {

	private TextChannel textChannel;
	
	public RatelierArmes(TextChannel textChannel) {
		super("\t>> Il semblerait que vous �tes arriv� dans l'ancienne armurerie du donjon.", 20);
		this.textChannel = textChannel;
	}

	public void effect(Personnage personnage) {
		Printer.sendMessage(textChannel, this.annonce);
		Random alea = new Random();
		int chance = alea.nextInt(100);
		if(chance < 33) {
			if(personnage instanceof Magicien) {
				Printer.sendMessage(textChannel, "\t>> Vous trouvez un b�ton simple et le mettez dans votre inventaire.");
				personnage.add(new BatonSimple(), textChannel);
			}
			if(personnage instanceof Chevalier) {
				Printer.sendMessage(textChannel, "\t>> Vous trouvez une �p�e simple et la mettez dans votre inventaire.");
				personnage.add(new EpeeSimple(), textChannel);
			}
		}
		if(chance >= 33 && chance < 66) {
			Printer.sendMessage(textChannel, "\t>> Vous trouvez un arc simple et le mettez dans votre inventaire.");
			personnage.add(new ArcSimple(), textChannel);
		}
		if(chance >= 66 && chance < 83) {
			if(personnage instanceof Magicien) {
				Printer.sendMessage(textChannel, "\t>> Vous trouvez un b�ton avanc� et le mettez dans votre inventaire.");
				personnage.add(new BatonAvance(), textChannel);
			}
			if(personnage instanceof Chevalier) {
				Printer.sendMessage(textChannel, "\t>> Vous trouvez une �p�e avanc�e et la mettez dans votre inventaire.");
				personnage.add(new EpeeAvance(), textChannel);
			}
		}
		if(chance >= 83) {
			Printer.sendMessage(textChannel, "\t>> Vous trouvez un arc avanc� et le mettez dans votre inventaire.");
			personnage.add(new ArcAvance(), textChannel);
		}
	}
}
