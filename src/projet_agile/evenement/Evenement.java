package projet_agile.evenement;

import projet_agile.personnage.Personnage;

public abstract class Evenement {
	
	protected String annonce;
	protected int numero;
	
	public Evenement (String ann, int num) {
		annonce = ann;
		numero = num;
	}
	

	public void effect(Personnage personnage) {}	
	
	public int getNumero() {
		return numero;
	}

}
