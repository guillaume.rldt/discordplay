package projet_agile.objet;

public class PotionMoyenne extends Potion {

	public PotionMoyenne() {
		soin = 40;
		nom = "une potion avanc�e";
	}
	@Override
	public String description() {
		return "Ceci est une potion avanc�e";
	}

	@Override
	public String getNom() {
		return nom;
	}
	
	@Override
	public String toString() {
		return "potion avanc�e (40PV)";
	}
}
