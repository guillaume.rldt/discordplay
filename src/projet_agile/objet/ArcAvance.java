package projet_agile.objet;

public class ArcAvance extends Arme {

	ArcAvance(int hp, int pa, int fo, int ad, int intel, int tri) {
		super(hp, pa, fo, ad, intel, tri);
		nom = "un arc avanc�";
		
	}
	
	public ArcAvance() {
		this(0, 0, 10, 10, 0, 0);
	}

	@Override
	public String description() {
		String msg = "Ceci est un arc avanc�";
		return msg;
	}
	
	@Override
	public String getNom() {
		return nom;
	}
	
	@Override
	public String toString() {
		return "arc avanc� (+10 FORCE, +10 AGILITE)";
	}
}
