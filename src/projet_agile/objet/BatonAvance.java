package projet_agile.objet;

public class BatonAvance extends Arme {

	public BatonAvance(int hp, int pa, int fo, int ad, int intel, int tri) {
		super(hp, pa, fo, ad, intel, tri);
		nom = "un baton avanc�";
		
	}

	public BatonAvance() {
		this(0, 0, 0, 0, 10, 0);

	}

	@Override
	public String description() {
		return "Ceci est un baton avanc�";
	}

	@Override
	public String toString() {

		return "baton avanc� (+10 INTELLIGENCE)";

	}

	@Override
	public String getNom() {
		return nom;
	}
}