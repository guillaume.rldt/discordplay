package projet_agile.objet;

public abstract class Arme extends Objet{
	private int hp;
	private int pa;//Points Actions
	private int fo;//force = +degats
	private int ad;//adresse = +precision/esquive 
	private int intel;//intellignece = +pa
	private int tri;//trippe = +sante/
	
	Arme(int hp, int pa, int fo, int ad, int intel, int tri) {
		this.hp = hp;
		this.fo = fo;
		this.ad = ad;
		this.intel = intel;
		this.tri = tri;
		
	}
	
	public int getHp() {
		return hp;
	}
	public void setHp(int hp) {
		this.hp = hp;
	}
	public int getPa() {
		return pa;
	}
	public void setPa(int pa) {
		this.pa = pa;
	}
	public int getFo() {
		return fo;
	}
	public void setFo(int fo) {
		this.fo = fo;
	}
	public int getAd() {
		return ad;
	}
	public void setAd(int ad) {
		this.ad = ad;
	}
	public int getIntel() {
		return intel;
	}
	public void setIntel(int intel) {
		this.intel = intel;
	}
	public int getTri() {
		return tri;
	}
	public void setTri(int tri) {
		this.tri = tri;
	}
	
}
