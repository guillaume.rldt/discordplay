package projet_agile.objet;

public class EpeeSacre extends Arme {

	EpeeSacre(int hp, int pa, int fo, int ad, int intel, int tri) {
		super(hp, pa, fo, ad, intel, tri);
		nom = "une �p�e sacr�e";
		
	}
	
	public EpeeSacre() {
		this(0, 0, 20, 0, 0, 0);
		
	}

	@Override
	public String description() {
		String msg = "Ceci est l'�p�e sacr�e";
		return msg;
	}

	@Override
	public String getNom() {
		return nom;
	}

	@Override
	public String toString() {
		return "�p�e sacr�e";
	}
}