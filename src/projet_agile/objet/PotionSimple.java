package projet_agile.objet;

public class PotionSimple extends Potion{

	public PotionSimple() {
		soin = 20;
		nom = "une potion simple";
	}
	@Override
	public String description() {
		return "Ceci est une potion simple";
	}

	@Override
	public String getNom() {
		return nom;
	}
	
	@Override
	public String toString() {
		return "potion simple (20PV)";
	}
}
