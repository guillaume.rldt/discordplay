package projet_agile.objet;

public class PotionGeante extends Potion {


	public PotionGeante() {
		soin = 60;
		nom = "une m�ga potion";
	}
	@Override
	public String description() {
		return "Ceci est une m�ga potion";
	}

	@Override
	public String getNom() {
		return nom;
	}
	
	@Override
	public String toString() {
		return "m�ga potion (60PV)";
	}
}
