package projet_agile.objet;

public class EpeeAvance extends Arme {

	EpeeAvance(int hp, int pa, int fo, int ad, int intel, int tri) {
		super(hp, pa, fo, ad, intel, tri);
		nom = "une �p�e avanc�e";
		
	}
	
	public EpeeAvance() {
		this(0, 0, 10, 0, 0, 0);
	}

	@Override
	public String description() {
		String msg = "Ceci est une �p�e avanc�e";
		return msg;
	}
	
	@Override
	public String getNom() {
		return nom;
	}
	
	@Override
	public String toString() {
		return "�p�e avanc�e (+10 FORCE)";
	}
	
}
