package projet_agile.objet;

public class EpeeSimple extends Arme {

	EpeeSimple(int hp, int pa, int fo, int ad, int intel, int tri) {
		super(hp, pa, fo, ad, intel, tri);
		nom = "une �p�e simple";
		
	}
	
	public EpeeSimple() {
		this(0, 0, 5, 0, 0, 0);
		
	}

	@Override
	public String description() {
		String msg = "Ceci est une �p�e simple";
		return msg;
	}

	@Override
	public String getNom() {
		return nom;
	}

	@Override
	public String toString() {
		return "�p�e simple (+5 FORCE)";
	}
}
