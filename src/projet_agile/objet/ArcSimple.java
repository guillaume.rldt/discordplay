package projet_agile.objet;

public class ArcSimple extends Arme{
	
	ArcSimple(int hp, int pa, int fo, int ad, int intel, int tri) {
		super(hp, pa, fo, ad, intel, tri);
		nom = "un arc simple";
		
	}
	
	public ArcSimple() {
		this(0, 0, 5, 5, 0, 0);
	}

	@Override
	public String description() {
		String msg = "Ceci est un arc simple";
		return msg;
	}
	
	@Override
	public String getNom() {
		return nom;
	}
	
	@Override
	public String toString() {
		return "arc simple (+5 FORCE, +5 AGILITE)";
	}
}
