package projet_agile.objet;

public class BatonSimple extends Arme {

	public BatonSimple(int hp, int pa, int fo, int ad, int intel, int tri) {
		super(hp, pa, fo, ad, intel, tri);
		nom = "un b�ton simple";
		
	}

	public BatonSimple() {
		this(0, 0, 0, 0, 5, 0);

	}

	@Override
	public String description() {
		return "Ceci est un b�ton simple";
	}

	@Override
	public String toString() {

		return "b�ton simple (+5 INTELLIGENCE)";

	}

	@Override
	public String getNom() {
		return nom;
	}
}
