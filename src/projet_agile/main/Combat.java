package projet_agile.main;

import net.dv8tion.jda.api.entities.TextChannel;
import projet_agile.personnage.*;
import utils.Printer;
import utils.Utils;

public class Combat {

	public static void combat(Personnage p1, Personnage p2, TextChannel textChannel, Utils utils) {
		boolean tourJoueur = true;
		
		Printer.sendMessage(textChannel, "\t>> Nouveau Combat !!\n" + p2.toString());
		
		while (!p1.estMort() && !p2.estMort()) {
			if (tourJoueur) {
				Printer.sendMessage(textChannel, "\t>> Voulez-vous attaquer ? (y/n)");
				switch (utils.readString()) {
				case "y":
				case "a":
				case "yes":
					Printer.sendMessage(textChannel, p1.attaque(p2));
					break;

				default:
					break;
				}
				tourJoueur = false;
			} else {
				Printer.sendMessage(textChannel, p2.attaque(p1));
				tourJoueur = true;
			}
		}
	}
}
