package music;

import java.util.HashMap;
import java.util.Map;

import com.sedmelluq.discord.lavaplayer.player.AudioLoadResultHandler;
import com.sedmelluq.discord.lavaplayer.player.AudioPlayerManager;
import com.sedmelluq.discord.lavaplayer.player.DefaultAudioPlayerManager;
import com.sedmelluq.discord.lavaplayer.player.FunctionalResultHandler;
import com.sedmelluq.discord.lavaplayer.source.AudioSourceManagers;
import com.sedmelluq.discord.lavaplayer.tools.FriendlyException;
import com.sedmelluq.discord.lavaplayer.track.AudioPlaylist;
import com.sedmelluq.discord.lavaplayer.track.AudioTrack;

import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.TextChannel;
import utils.Printer;

public class MusicManager {
	private final AudioPlayerManager manager = new DefaultAudioPlayerManager();
	private final Map<String, MusicPlayer> players = new HashMap<>();
	
	public MusicManager() {
		AudioSourceManagers.registerRemoteSources(manager);
		AudioSourceManagers.registerLocalSource(manager);
	}
	
	public synchronized MusicPlayer getPlayer(Guild guild) {
		if(!players.containsKey(guild.getId())) players.put(guild.getId(), new MusicPlayer(manager.createPlayer(), guild));
		return players.get(guild.getId());
	}
	
	public void loadTracks(final TextChannel channel, final String source) {
		MusicPlayer player = getPlayer(channel.getGuild());
		
		channel.getGuild().getAudioManager().setSendingHandler(player.getAudioHandler());
		
		manager.loadItemOrdered(player, source, new AudioLoadResultHandler() {

			@Override
			public void trackLoaded(AudioTrack track) {
				player.playTrack(track);
				Printer.sendMessage(channel, "Added to queue " + track.getInfo().title);
			}

			@Override
			public void playlistLoaded(AudioPlaylist playlist) {
				Printer.sendMessage(channel, "isn't implement yet");
				
			}

			@Override
			public void noMatches() {
				Printer.sendMessage(channel, source + " no matches");
			}

			@Override
			public void loadFailed(FriendlyException exception) {
				Printer.sendMessage(channel, "Impossible to load reason : " + exception.getMessage() + ")");
			}
			
		});
	}
}
