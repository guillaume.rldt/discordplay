package music;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import com.sedmelluq.discord.lavaplayer.player.AudioPlayer;
import com.sedmelluq.discord.lavaplayer.player.event.AudioEventAdapter;
import com.sedmelluq.discord.lavaplayer.track.AudioTrack;
import com.sedmelluq.discord.lavaplayer.track.AudioTrackEndReason;

public class AudioListener extends AudioEventAdapter {
	private final BlockingQueue<AudioTrack> tracks = new LinkedBlockingQueue<>();
	private final MusicPlayer player;

	public AudioListener(MusicPlayer player) {
		this.player = player;
	}

	public BlockingQueue<AudioTrack> getTracks() {
		return tracks;
	}

	public int getTracksSize() {
		return tracks.size();
	}

	public void nextTracks() {
		if(tracks.isEmpty()) {
			Thread wait = new Thread() {
				public void run() {
					try {
						Thread.sleep(30000);
					} catch (InterruptedException e) {}
					if(tracks.isEmpty()) {
						player.getGuild().getAudioManager().closeAudioConnection();
					}
				}
			};
			wait.run();
			return;
		}
		player.getAudioPlayer().startTrack(tracks.poll(), false);
	}

	@Override
	public void onTrackEnd(AudioPlayer player, AudioTrack track, AudioTrackEndReason endReason) {
		if (endReason.mayStartNext) nextTracks();
	}

	public void queue(AudioTrack track) {
		if (!player.getAudioPlayer().startTrack(track, true)) tracks.offer(track);
	}
}
