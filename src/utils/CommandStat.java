package utils;

public enum CommandStat {

	SUCCESSED ("Command successed"),
	FAILED ("Command failed"),
	EXECUTION_FAILED ("Execution failed"),
	UNKNOW_COMMAND ("Unknow command"),
	WIP_COMMAND ("Work In Progress");
	
	private String message;
	
	CommandStat(String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}
}
