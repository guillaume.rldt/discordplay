package utils;

import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.entities.User;

public interface CommandExecutor {
	public CommandStat executeCommand(Guild guild, TextChannel textChannel, User user, String args);
}
