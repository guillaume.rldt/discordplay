package utils;

import net.dv8tion.jda.api.entities.TextChannel;

public class Printer {
	public static void sendMessage(TextChannel textChannel, String args) {
		try {
			textChannel.sendMessage("```" + args + "```").queue();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}
