package utils;

import javax.security.auth.login.LoginException;

import main.Bot;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.entities.User;

public class Utils {

	private Thread readString;
	private MessageListener ml;
	private User user;
	private TextChannel textChannel;
	private Guild guild;
	
	/*
	public static String readString() {
		readString = new Thread() {
			public void run() {
				ml = new MessageListener(readString, null, null, null);
			}
		};
		readString.start();
		while (!readString.isInterrupted()) {}
		String res = ml.getMessage();
		return res;
	}*/

	public Utils(User user, TextChannel textChannel, Guild guild) {
		this.user = user;
		this.textChannel = textChannel;
		this.guild = guild;		
		readString = new Thread() {
			public void run() {
				try {
					JDABuilder.createDefault(Bot.getBotToken())
					.addEventListeners(ml = new MessageListener(user, textChannel, guild))
					.build();
				} catch (LoginException e) {
					e.printStackTrace();
				}
				
			}
		};
		readString.start();
	}
	
	public String readString() {
		
		while (!ml.isReceved()) {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		String res = ml.getMessage();
		try {
			readString.sleep(10);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return res;
	}

	public User getUser() {
		return user;
	}

	public TextChannel getTextChannel() {
		return textChannel;
	}

	public Guild getGuild() {
		return guild;
	}
}