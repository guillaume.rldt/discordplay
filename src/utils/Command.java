package utils;

import main.Bot;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.entities.User;
import commands.*;

public enum Command {
	CLEAR("CLEAR", "Syntax : "+ Bot.BOT_PREFIX + "CLEAR", new ClearCommand()),
	HELP("HELP", "Syntax : "+ Bot.BOT_PREFIX + "HELP [COMMAND-NAME]", new HelpCommand()),
	PLAY("PLAY", "Syntax : "+ Bot.BOT_PREFIX + "PLAY [URL|TAGS]", new PlayCommand()),
	QUIT("QUIT", "Syntax : "+ Bot.BOT_PREFIX + "QUIT", new QuitCommand()),
	SKIP("SKIP", "Syntax : "+ Bot.BOT_PREFIX + "SKIP", new SkipCommand()),
	GAME("GAME", "Syntax : "+ Bot.BOT_PREFIX + "GAME", new GameCommand()),
	;
	
	private String command;
	private String help;
	private CommandExecutor exe;
	
	private Command(String command) {
		this(command, "No help found");
	}
	
	private Command(String command, String help) {
		this.command = command;
		this.help = help;
		this.exe = new WIPCommand();
	}
	
	private Command(String command, String help, CommandExecutor exe) {
		this.command = command;
		this.help = help;
		this.exe = exe;
	}
	
	public String getCommand() {
		return command;
	}

	public String getHelp() {
		return help;
	}
	
	public CommandStat execute(Guild guild, TextChannel textChannel, User user, String args) {
		return exe.executeCommand(guild, textChannel, user, args);
	}
}