package utils;

import game.*;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.entities.User;

public enum Game {
	TEXTRPG("TEXTRPG", new TextRpgGame()),
	TICTACTOE("TICTACTOE", new TicTacToeGame()),
	;
	
	private String game;
	private GameExecutor exe;
	
	private Game(String game) {
		this(game, new WIPGame());
	}
	
	private Game(String command, GameExecutor exe) {
		this.game = command;
		this.exe = exe;
	}
	
	public String getGame() {
		return game;
	}
	
	public GameStat execute(Guild guild, TextChannel textChannel, User user, String args) {
		return exe.execute(guild, textChannel, user, args);
	}
}
