package utils;

public enum GameStat {

	FINISHED ("Game finished"),
	PENDING ("Game pending");
	
	private String message;
	
	GameStat(String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}
}
