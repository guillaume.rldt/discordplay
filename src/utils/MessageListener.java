package utils;

import main.Bot;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

public class MessageListener extends ListenerAdapter{

	private boolean receved = false;
	
	private User user;
	private TextChannel textChannel;
	private Guild guild;
	private String message;

	public MessageListener() {
		super();
	}

	public MessageListener(User user) {
		this();
		this.user = user;
	}

	public MessageListener( User user, TextChannel textChannel) {
		this(user);
		this.textChannel = textChannel;
	}

	public MessageListener(User user, TextChannel textChannel, Guild guild) {
		this(user, textChannel);
		this.guild = guild;
	}

	@Override
	public void onGuildMessageReceived(GuildMessageReceivedEvent event) 
	{
		if(event.getMessage().getContentRaw().startsWith(Bot.BOT_PREFIX)) return;
		
		Guild guild = event.getGuild();
		TextChannel textChannel = event.getChannel();
		User user = event.getAuthor();
		
		if(event.getAuthor().isBot()) return;
		
		if(this.guild != null) {
			if(!guild.equals(this.guild)) return;
		}
		
		if(this.textChannel != null) {
			if(!textChannel.equals(this.textChannel)) return;
		}
		
		if(this.user != null) {
			if(!user.equals(this.user)) return;
		}
			
		message = event.getMessage().getContentRaw();
		receved = true;
	}

	public String getMessage() {
		receved = false;
		return message;
	}

	public boolean isReceved() {
		return receved;
	}
}