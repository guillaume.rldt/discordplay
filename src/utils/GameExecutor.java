package utils;

import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.entities.User;

public interface GameExecutor {
	public GameStat execute(Guild guild, TextChannel textChannel, User user, String args);
}
