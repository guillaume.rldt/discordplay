package commands;

import main.Bot;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.entities.VoiceChannel;
import utils.CommandExecutor;
import utils.CommandStat;

public class PlayCommand implements CommandExecutor {

	@Override
	public CommandStat executeCommand(Guild guild, TextChannel textChannel, User user, String args) {
		if(guild == null) return CommandStat.FAILED;

		if(args.length() == 0) {
			textChannel.sendMessage("Missing args.");
			return CommandStat.FAILED;
		}

		if(!guild.getAudioManager().isConnected() && !guild.getAudioManager().isAttemptingToConnect()) {
			VoiceChannel voiceChannel = guild.getMember(user).getVoiceState().getChannel();
			if(voiceChannel == null) {
				textChannel.sendMessage("You have to be in a voice channel to use this command.");
				return CommandStat.FAILED;
			}

			guild.getAudioManager().openAudioConnection(voiceChannel);
		}

		System.out.println(args);

		if(args.startsWith("https://")) Bot.manager.loadTracks(textChannel, args);
		else {
			//Bot.manager.loadItem("ytsearch: epic soundtracks", new FunctionalResultHandler(null, playlist -> {
			//      player.playTrack(playlist.getTracks().get(0));
			//    }, null, null));
		}

		return CommandStat.SUCCESSED;
	}
}