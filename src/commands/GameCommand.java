package commands;

import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.entities.User;
import utils.CommandExecutor;
import utils.CommandStat;
import utils.Game;
import utils.Printer;

public class GameCommand implements CommandExecutor  {

	@Override
	public CommandStat executeCommand(Guild guild, TextChannel textChannel, User user, String args) {

		String message = "";

		if (args.length() == 0) {
			message = "what game do you want to play ?\n" + showGameAvailiable();
			Printer.sendMessage(textChannel, message);
		}
		else {
			Thread game = new Thread() {
				public void run() {
					Game.valueOf(args.split(" ")[0].toUpperCase()).execute(guild, textChannel, user, args);
				}
			};
			game.start();

		}
		return CommandStat.SUCCESSED;
	}

	private String showGameAvailiable() {
		String message = "";
		for (Game game : Game.values()) {
			message += game.getGame() + "\n";
		}
		return message;
	}
}
