package commands;

import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.entities.User;
import utils.CommandExecutor;
import utils.CommandStat;

public class WIPCommand implements CommandExecutor {

	@Override
	public CommandStat executeCommand(Guild guild, TextChannel textChannel, User user, String args) {
		return CommandStat.WIP_COMMAND;
	}
	
}
