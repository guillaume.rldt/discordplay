package commands;

import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.entities.User;
import utils.Command;
import utils.CommandExecutor;
import utils.CommandStat;
import utils.Printer;

public class HelpCommand implements CommandExecutor {

	@Override
	public CommandStat executeCommand(Guild guild, TextChannel textChannel, User user, String args) {
		String message = "";
		if (args.length() == 0) {
			message = allCommand();
		}
		else {
			String[] argsSplited = args.split(" ");
			if(argsSplited.length > 1) return CommandStat.FAILED;
			message = showHelpOnOneCommand(argsSplited[0]);
		}
		Printer.sendMessage(textChannel, message);
		return CommandStat.SUCCESSED;
	}

	private String allCommand() {
		String message = "The following commands are recognized:\n";
		for(Command cmd : Command.values())
			message += " " + cmd.getCommand() + " " + cmd.getHelp() + "\n";
		return message;
	}

	public static String showHelpOnOneCommand(String arg) {
		Command cmd = Command.valueOf(arg.toUpperCase());
		return cmd.getCommand() + " " + cmd.getHelp();
	}

}
