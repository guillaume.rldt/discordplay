package commands;

import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.entities.User;
import utils.CommandExecutor;
import utils.CommandStat;

public class QuitCommand implements CommandExecutor  {

	@Override
	public CommandStat executeCommand(Guild guild, TextChannel textChannel, User user, String args) {
		guild.getAudioManager().closeAudioConnection();
		textChannel.sendMessage("Disconnect.").queue();
		return CommandStat.SUCCESSED;
	}
}
