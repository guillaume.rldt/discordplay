package main;

import java.time.LocalDateTime;

import javax.security.auth.login.LoginException;

import commands.HelpCommand;
import music.MusicManager;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.exceptions.RateLimitedException;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import utils.Command;
import utils.CommandStat;
import utils.Printer;

public class Bot extends ListenerAdapter 
{
	public final static String BOT_PREFIX = "/";
	private final static String BOT_TOKEN = "NzAyMTEzODY2MjM4MjYzMzE2.Xp7aHw.D_zd4FEXyf1eSja7_K-bodvhAxM";
	public final static MusicManager manager = new MusicManager();

	public static void main(String[] args)
			throws IllegalArgumentException, LoginException, RateLimitedException
	{
		JDABuilder.createDefault(BOT_TOKEN)
		.addEventListeners(new Bot())
		.build();
	}

	@Override
	public void onGuildMessageReceived(GuildMessageReceivedEvent event) 
	{

		if (event.getAuthor().isBot()) return;
		if (!event.getMessage().getContentRaw().startsWith(BOT_PREFIX)) return;

		Guild guild = event.getGuild();
		TextChannel textChannel = event.getChannel();
		User user = event.getAuthor();

		String message = event.getMessage().getContentRaw();
		System.out.println("Message received : " + message + " by : " + event.getAuthor().getName() + " at : " + LocalDateTime.now());

		String cmd = message.replaceFirst(BOT_PREFIX, "");
		String[] cmdFragements = cmd.split(" ");

		String args = cmd.replaceFirst(cmdFragements[0], "");
		args = args.replaceFirst(" ", "");
		try {
			CommandStat cs = Command.valueOf(cmdFragements[0].toUpperCase()).execute(guild, textChannel, user, args);
			if(cs.equals(CommandStat.FAILED)) {
				Printer.sendMessage(textChannel, cs.getMessage() + "\n" + HelpCommand.showHelpOnOneCommand(cmdFragements[0].toUpperCase()));
			}
		} catch (Exception e) {}
	}

	public static String getBotToken() {
		return BOT_TOKEN;
	}
}