package game;

import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.entities.User;
import tic_tac_toe.GameMain;
import utils.GameExecutor;
import utils.GameStat;
import utils.Printer;
import utils.Utils;

public class TicTacToeGame implements GameExecutor {

	@Override
	public GameStat execute(Guild guild, TextChannel textChannel, User user, String args) {
		Utils p1 = new Utils(user, textChannel, guild);

		Utils p2 =  null;

		do {
			Printer.sendMessage(textChannel, "Who do you want to play with ?");
			try {
				p2 = new Utils(guild.getMemberById(p1.readString().replace("<@!", "").replace(">", "")).getUser(), textChannel, guild);
			} catch (Exception e) {
				Printer.sendMessage(textChannel, "You can't play with this user");
			}
			System.out.println(p2);
		} while (p2 == null);

		new GameMain(p1, p2, textChannel);
		
		return null;
	}

}
