package game;

import java.awt.Point;
import java.util.concurrent.TimeUnit;

import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.entities.User;

import projet_agile.donjon.*;
import projet_agile.main.*;
import projet_agile.personnage.*;

import utils.GameExecutor;
import utils.GameStat;
import utils.Printer;
import utils.Utils;

public class TextRpgGame implements GameExecutor {

	private static Personnage p1;
	private static Donjon donjon;
	private String buffer = "";
	
	@Override
	public GameStat execute(Guild guild, TextChannel textChannel, User user, String args) {
		Utils utils = new Utils(user, textChannel, guild);
		
		donjon = new Donjon(textChannel, utils);
		p1 = null;
		String classe = "";
		buffer = "";

		buffer =  "\t\t__________________________  ______________     ____________________  ________ \n"
				+ "\t\t\\__    ___/\\_   _____/\\   \\/  /\\__    ___/     \\______   \\______   \\/  _____/ \n"
				+ "\t\t  |    |    |    __)_  \\     /   |    |         |       _/|     ___/   \\  ___ \n"
				+ "\t\t  |    |    |        \\ /     \\   |    |         |    |   \\|    |   \\    \\_\\  \\\n"
				+ "\t\t  |____|   /_______  //___/\\  \\  |____|         |____|_  /|____|    \\______  /\n"
				+ "\t\t                   \\/       \\_/                        \\/                  \\/";

		buffer += "\n\n\n\n\n\n" + "\t\t\t|\\             //                             /^\\\n"
				+ "\t\t\t \\\\           _!_                        /\\   \"V\"\n"
				+ "\t\t\t  \\\\         /___\\                      /__\\   I\n"
				+ "\t\t\t   \\\\        [+++]                     //..\\\\  I\n"
				+ "\t\t\t    \\\\    _ _\\^^^/                     \\].`[/  I\n"
				+ "\t\t\t     \\\\/ (    '-'  ( )                 /l\\/j\\  (]\n"
				+ "\t\t\t     /( \\/ | {&}   /\\ \\               /. ~~ ,\\/I\n"
				+ "\t\t\t       \\  / \\     / _> )              \\\\L__j^\\/I\n"
				+ "\t\t\t        \"`   >:::;-'`\"\"'-.             \\/--v}  I\n"
				+ "\t\t\t            /:::/         \\            |    |  I\n"
				+ "\t\t\t           /  /||   {&}   |            |    |  I\n"
				+ "\t\t\t          (  / (\\         /            |    l  I\n"
				+ "\t\t\t          / /   \\'-.___.-'           _/j  L l\\_!\n"
				+ "\t\t\t        _/ /     \\ \\\n"
				+ "\t\t\t       /___|    /___|\n\n"
				+ "\t\t\t        61 HP                           44 HP\n"
				+ "\t\t\t        60 FORCE                        20 FORCE\n"
				+ "\t\t\t        25 ADRESSE                      20 ADRESSE\n"
				+ "\t\t\t        2 INTELLIGENCE                  80 INTELLIGENCE\n"
				+ "\t\t\t        60 COURAGE                      20 COURAGE\n";

		buffer += "\n\t\t  >> Qui souhaitez vous incarner, le chevalier ou le magicien ? \n";

		Printer.sendMessage(textChannel, buffer);
		buffer = "";


		classe = utils.readString();


		while (!(classe.equalsIgnoreCase("chevalier") || classe.equalsIgnoreCase("magicien"))) {
			buffer = "\t>> D�sol� mais ce n'est pas tout � fait exact !";
			Printer.sendMessage(textChannel, buffer);
			buffer = "";
			
			classe = utils.readString();
		}
		
		if (classe.equalsIgnoreCase("chevalier")) {
			p1 = new Chevalier();
		} else if (classe.equalsIgnoreCase("magicien")) {
			p1 = new Magicien();
		}
		
		buffer = "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n" + "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n" + p1 + "\n"
				+ "\t>> Bien ! Maintenant il me faut votre nom !";
		Printer.sendMessage(textChannel, buffer);
		buffer = "";
		
		String nom = utils.readString();
		
		buffer = "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n" + "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n" + p1 + "\n"
				+ "\t>> Bienvenue dans l'antre de la b�te, un terrible dragon hante ces lieux, terrorisant la population avec l'aide de ses sbires.\n\t>> Il ne tient qu'� vous de mettre fin � son r�gne de terreur et � purger ses lieux impies.";
		buffer += "\n\n\t>> Pr�parez-vous, votre aventure va commencer";
		Printer.sendMessage(textChannel, buffer);
		buffer = "";
		
		try {
			TimeUnit.SECONDS.sleep(2);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		buffer = donjon.toString();
		Printer.sendMessage(textChannel, buffer);
		buffer = "";
		
		boolean finPartieBoss = false, finPartieCase = false;

		do {

			if(donjon.getSalleActuelle().contientEvenement()) {
				donjon.getSalleActuelle().getEvenement().effect(p1);
				donjon.getSalleActuelle().supprimerEvenement();
			}

			if (donjon.getSalleActuelle().contientEnnemi()) {
				if (donjon.getSalleActuelle().salleBoss) {
					buffer = "\t>> Vous venez d'entrer dans la salle du boss !\n\t>> Le combat s'annonce difficile!\n";
					Printer.sendMessage(textChannel, buffer);
					buffer = "";
					try {
						TimeUnit.SECONDS.sleep(3);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}

				}

				buffer = "\t>> Attention il y a "+donjon.getSalleActuelle().getNombreEnnemi()+" ennemi(s) dans cette salle !\n";
				Printer.sendMessage(textChannel, buffer);
				buffer = "";
				
				try {
					TimeUnit.SECONDS.sleep(1);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}

				Combat.combat(p1, donjon.getSalleActuelle().getMonstre(), textChannel, utils);

				if (donjon.getSalleActuelle().getMonstre().estMort()) {

					donjon.getSalleActuelle().mortEnnemi();

					if (!donjon.getSalleActuelle().contientEnnemi()) {

						buffer = "\t>> Combat termin� !\n\n";
						Printer.sendMessage(textChannel, buffer);
						buffer = "";
						if (donjon.getSalleActuelle().salleBoss) {
							finPartieBoss = true;
							buffer = "Felicitation vous avez triomph� de cet horrible boss !\n";
							Printer.sendMessage(textChannel, buffer);
							buffer = "";
							if (!finPartieCase) {
								buffer = "Mais il reste des salles inexplor�es dans le donjon, vous devez les d�couvrir pour vous enfuir.\n";
								Printer.sendMessage(textChannel, buffer);
								buffer = "";
							}
						}
					} else {
						buffer = "\t>> Pas de temps � perdre, le combat continu !\n";
						Printer.sendMessage(textChannel, buffer);
						buffer = "";
					}
				}
			} else {
				buffer = donjon.toString();
				Printer.sendMessage(textChannel, buffer);
				buffer = "";
				Printer.sendMessage(textChannel, "\t>> Que voulez vous faire ? \n\t>> Z,Q,S,D pour se d�placer, F pour fouiller, I pour inventaire !");
				traitementDesCommandes(utils.readString(), textChannel, utils);
			}
			if (donjon.toutVisite()) {
				finPartieCase = true;
				buffer = "Vous avez visit� tout le donjon et terassez le boss !\nVous voila victorieux et pr�t � quitter ce donjon !\n";
				Printer.sendMessage(textChannel, buffer);
				buffer = "";
			}
		} while (!p1.estMort() && (!finPartieBoss && !finPartieCase));
		victoire(guild, textChannel, user, args);
		return GameStat.FINISHED;
	}

	private boolean traitementDesCommandes(String commande, TextChannel textChannel, Utils utils) {
		Point positionActuel = donjon.chercherSalle(donjon.getSalleActuelle().getNumeroSalle());
		Point positionTmp;
		buffer = "";
		
		donjon.getSalleActuelle().estVisite = true;
		switch (commande) {
		case "s":
		case "sud":

			if (positionActuel.x + 1 > donjon.getSallesDuDonjon().length - 1)

				return false;
			positionTmp = new Point(positionActuel.x + 1, positionActuel.y);
			if (donjon.getSallesDuDonjon()[positionTmp.x][positionTmp.y] == null)
				return false;
			donjon.setSalleActuelle(donjon.getSallesDuDonjon()[positionTmp.x][positionTmp.y]);
			return true;
		case "z":
		case "n":
		case "nord":
			if (positionActuel.x - 1 < 0)
				return false;
			positionTmp = new Point(positionActuel.x - 1, positionActuel.y);
			if (donjon.getSallesDuDonjon()[positionTmp.x][positionTmp.y] == null)
				return false;
			donjon.setSalleActuelle(donjon.getSallesDuDonjon()[positionTmp.x][positionTmp.y]);
			break;
		case "d":
		case "e":
		case "est":

			if (positionActuel.y + 1 > donjon.getSallesDuDonjon()[0].length - 1)

				return false;
			positionTmp = new Point(positionActuel.x, positionActuel.y + 1);
			if (donjon.getSallesDuDonjon()[positionTmp.x][positionTmp.y] == null)
				return false;
			donjon.setSalleActuelle(donjon.getSallesDuDonjon()[positionTmp.x][positionTmp.y]);
			break;
		case "q":
		case "o":
		case "ouest":
			if (positionActuel.x - 1 < 0)
				return false;
			positionTmp = new Point(positionActuel.x, positionActuel.y - 1);
			if (donjon.getSallesDuDonjon()[positionTmp.x][positionTmp.y] == null)
				return false;
			donjon.setSalleActuelle(donjon.getSallesDuDonjon()[positionTmp.x][positionTmp.y]);
			break;
		case "f":
		case "fouille":
		case "fouiller":
			p1.add(donjon.getSallesDuDonjon()[positionActuel.x][positionActuel.y].fouille(), textChannel);
			break;
		case "i":
		case "inventaire":
			buffer = p1.showInventaire();
			buffer += "\n\t>> Voulez vous utiliser un objet ?";
			Printer.sendMessage(textChannel, buffer);
			buffer = "";
			String commandeInv = utils.readString();
			try {
				p1.useObject(Integer.parseInt(commandeInv), textChannel);
			} catch (Exception e) {};
			break;
		default:
			return false;
		}
		return true;
	}

	private void victoire(Guild guild, TextChannel textChannel, User user, String args) {
		buffer = "";
		buffer = " __      _______ _____ _______ ____ _____ _____  ______      \r\n" + 
				 " \\ \\    / /_   _/ ____|__   __/ __ \\_   _|  __ \\|  ____| \r\n" + 
				 "  \\ \\  / /  | || |       | | | |  | || | | |__) | |__    \r\n" + 
				 "   \\ \\/ /   | || |       | | | |  | || | |  _  /|  __|   \r\n" + 
				 "    \\  /   _| || |____   | | | |__| || |_| | \\ \\| |____  \r\n" + 
				 "     \\/   |_____\\_____|  |_|  \\____/_____|_|  \\_\\______| \r\n" + 
				 "                                                         \r\n" + 
				 "                                                        ";
		Printer.sendMessage(textChannel, buffer);
		buffer = "";
	}





}

